const db = require("../config/db.js");
const Comment = db.comment;
const asyncMiddleware = require("express-async-handler");

//create comment
exports.addComment = asyncMiddleware(async (req, res) => {
    // Save comment to Database
    const comments = await Comment.create({
        bookId: req.params.id,
        userId: req.userId,
        comment: req.body.comment
    });
    res.status(201).send({
        status:  comments
    });
});

exports.getComment = asyncMiddleware(async (req, res) => {
    const book = await Comment.findAll({
        attributes: ["id", "comment"],
        include:[
            {
                model: Comment,
                attributes:['id','comment'],
                include:[
                    {
                        model: User,
                        attributes:['id','username']
                    }
                ]
            }
        ]
    });
    res.status(201).send({
        status: comment
    });
});